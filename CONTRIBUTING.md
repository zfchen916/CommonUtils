# 部署Nexus
由于其他项目是通过Maven依赖引入本项目的，而本项目是放在235的Nexus Maven2本地镜像，所以在提交代码后，如果不进行安装/部署，其他引用了CommonUtils的项目还是调用旧的版本。  
如果是**本地测试**，给本地的其他项目用，直接执行`mvn install`即可。  
如果是要**给其他人使用**，需要将新代码部署到Nexus，可以找 胡嘉宏 帮忙部署，也可以自己部署。  
部署的方法：修改系统的Maven配置文件(Linux:~/.m2/settings.xml，Windows：C:/Users/[你的用户名]/.m2/settings.xml)，在settings标签下增加servers元素如下：  
```xml
<settings xmlns="………">
    <!--…………-->
    <servers> 
        <server>  
            <id>Releases</id>  
            <username>deployment</username>  
            <password>deployment123</password>  
        </server>      
        <server>  
            <id>Snapshots</id>  
            <username>deployment</username>  
            <password>deployment123</password>  
        </server>
    </servers> 
    <!--…………-->
</settings>
```
然后进入CommonUtils项目目录，执行`mvn deploy`即可
