# 目录
* [简介](#user-content-简介)  
* [Maven依赖](#user-content-maven依赖)  
* [项目结构](#user-content-项目结构)  
* [部署Nexus(使用者无需阅读)](#user-content-部署nexus使用者无需阅读)  
* [工具类/方法详解](#user-content-工具类方法详解)  
&gt;&gt;&emsp;[basic包](#user-content-basic包)  
&gt;&gt;&emsp;[encrypt包](#user-content-encrypt包)  
&gt;&gt;&emsp;[format包](#user-content-format包)  
&gt;&gt;&emsp;[wechat包](#user-content-wechat包)  

# 简介
本项目旨在提取各项目中重复出现的一些工具类，并进行代码优化；其他项目可以直接引用本项目，调用其工具类方法。  
欢迎大家参与本项目，将有需要的工具类的加入到项目中，优化代码，[可参考部署方法](#user-content-部署nexus使用者无需阅读)。  

# Maven依赖
使用本项目时，增加Maven依赖需要修改pom.xml文件：
```xml
<!-- 增加235的Nexus镜像 -->
<project>
    <!--…………-->
    <repositories>
        <repository>
            <id>public</id>
            <name>public</name>
            <url>http://172.16.99.235:8081/nexus/content/groups/public/</url>
        </repository>
    </repositories>
    
    <!-- 增加CommonUtils项目依赖 -->
    <dependencies>
    <!--…………-->
        <dependency>
            <groupId>com.turingdi</groupId>
            <artifactId>commonutils</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
    <!--…………-->
    </dependencies>
    <!--…………-->
</project>
```

# 项目结构
本项目基于Maven构建，项目结构与一般的java项目一致。

其中包结构如下（点击包名/类名进入方法明细）：

| 包/类名 |     说明 |
|:------------- |:-------------|
| [com.turingdi.commonutils.basic](#user-content-basic包) | 基本工具(文件操作、网络操作等)，以及暂时难以分类的工具类。|
| &gt;&gt;&emsp;[PropertiesUtils类](#user-content-propertiesutils类) | properties配置文件相关的工具类，会自动加载项目根目录所有properties后缀的文件，通过文件名(无需后缀)和key就获取对应取值，支持获取String/int/double类型的值。|
| &gt;&gt;&emsp;[CommonUtils类](#user-content-commonutils类) | 一些最基本的工具类包，包括批量关闭资源，将字节数组转换为16进制的字符串，对Map按key的字典排序，使用StringBuffer进行字符串替换，判断所有字符串参数都非空，随机打乱数组，随机的UUID，Unicode字符串解码等方法。|
| &gt;&gt;&emsp;[FileUtils类](#user-content-fileutils类) | 文件操作相关的类，包括将字符串写入到文件，从文件读取字符串等方法。|
| &gt;&gt;&emsp;[NetworkUtils类](#user-content-networkutils类) | 网络操作相关的工具类，包括调用远程API接口(POST请求)、获取随机IP地址、下载网络文件等方法。|
| [com.turingdi.commonutils.encrypt](#user-content-encrypt包) | 加密相关的工具类包|
| &gt;&gt;&emsp;[MD5Utils类](#user-content-ms5utils类) | MD5工具类，包含获取MD5码字符串的方法。|
| &gt;&gt;&emsp;[SHA1Utils类](#user-content-sha1utils类) | SHA1工具类，包括获取SHA1码字符串、及用于签名验证的参数排序方法。|
| &gt;&gt;&emsp;[MurmurHash3Utils类](#user-content-murmurhash3utils类) | MurmurHash算法工具类，包括使用MurmurHash3算法获取32和64位哈希值的方法。|
| [com.turingdi.commonutils.format](#user-content-format包) | 格式转换的工具类包|
| &gt;&gt;&emsp;[DateUtils类](#user-content-dateutils类) | 提供获取常见日期格式字符串的方法，以及获取N小时前的时刻的方法。|
| &gt;&gt;&emsp;[ExcelUtils类](#user-content-excelutils类) | 提供写入数据到Excel文件的方法。|
| &gt;&gt;&emsp;[XmlUtils类](#user-content-xmlutils类) | 提供将键值对数据转换为xml格式的字符串，和将xml字符串转换为实体类对象的方法。|
| [com.turingdi.commonutils.wechat](#user-content-wechat包) | 微信接口调用的工具类包|
| &gt;&gt;&emsp;[QRCodeUtils类](#user-content-qrcodeutils类) | 二维码的工具类，提供生成二维码图片文件/输出流的方法。 |
| &gt;&gt;&emsp;[WechatRecharge类](#user-content-wechatrecharge类) | 微信充值的工具类，提供调用话费充值、订单查询、余额查询的方法。 |
| &gt;&gt;&emsp;[WaterMarkUtils类](#user-content-watermarkutils类) | 添加水印的工具类，提供了添加水印文字和水印图片的方法。 |
| &gt;&gt;&emsp;[WaterMarkParam类](#user-content-watermarkparam类) | 添加水印的参数类，该类封装了添加水印需要用到的参数。 |

# 部署Nexus(使用者无需阅读)
由于其他项目是通过Maven依赖引入本项目的，而本项目是放在235的Nexus Maven2本地镜像，所以在提交代码后，如果不进行安装/部署，其他引用了CommonUtils的项目还是调用旧的版本。  
如果是**本地测试**，给本地的其他项目用，直接执行`mvn install`即可。  
如果是要**给其他人使用**，需要将新代码部署到Nexus，可以找 胡嘉宏 帮忙部署，也可以自己部署。  
部署的方法：修改系统的Maven配置文件(Linux:~/.m2/settings.xml，Windows：C:/Users/[你的用户名]/.m2/settings.xml)，在settings标签下增加servers元素如下：  
```xml
<settings xmlns="………">
    <!--…………-->
    <servers> 
        <server>  
            <id>Releases</id>  
            <username>deployment</username>  
            <password>deployment123</password>  
        </server>      
        <server>  
            <id>Snapshots</id>  
            <username>deployment</username>  
            <password>deployment123</password>  
        </server>
    </servers> 
    <!--…………-->
</settings>
```  
**切记，执行`mvn deploy`之前请务必先`git pull`，以保证部署的是最新版本**  
然后进入CommonUtils项目目录，执行`mvn deploy`即可

# 工具类/方法详解
## basic包
基本工具(文件操作、网络操作等)，以及暂时难以分类的工具类。
### PropertiesUtils类
properties配置文件相关的工具类，第一次引用时会自动加载（静态代码块调用）项目根目录所有properties后缀的文件，通过文件名(无需后缀)和key获取对应取值，支持获取String/int/double类型的值。
#### getString(String, String)方法
**签名：** `public static String getString(String filename, String key)`  
**功能：** 获取filename.properties文件中key对应的String值  
**参数：**  
* filename: 配置文件名字，无需后缀（.properties）
* key: 配置文件键值对的键key

**例子：**  
```java
PropertiesUtils.getString("hbase","ZKQuorum"); //huoqu hbase.properties文件的ZKQuorum字段， 即CentOS03:2181
```

#### getInt(String, String)方法
**签名：** `public static String getInt(String filename, String key)`  
**功能：** 获取filename.properties文件中key对应的int值  
**参数：**  
* filename: 配置文件名字，无需后缀（.properties）
* key: 配置文件键值对的键key

#### getDouble(String, String)方法
**签名：** `public static String getDouble(String filename, String key)`  
**功能：** 获取filename.properties文件中key对应的double值  
**参数：**  
* filename: 配置文件名字，无需后缀（.properties）
* key: 配置文件键值对的键key

---
### CommonUtils类
该类存放一些最基本的工具类。
#### closeResources(Closeable...)方法
**签名：** `public static void closeResources(Closeable... resources)`  
**功能：** 批量关闭资源，并捕捉异常写入日志，避免在finally块中大段的判断非空关闭资源  
**参数：**
* resources: Closeable接口，待关闭的资源，支持多参数

**例子：**  
原来的写法：

```java
Connection connection = null;
Table table = null;
try {
    //业务代码
} catch (IOException e) {
    LOG.info("…………" + e.getMessage());
} finally {
    try {
        if(null != table)
            table.close();
        if(null != connection)
            connection.close();
    } catch (IOException e) {
        LOG.info("…………" + e.getMessage());
    }
}
```

使用CommonUtils：  
```java
Connection connection = null;
Table table = null;
try {
    //业务代码
} catch (IOException e) {
    LOG.info("…………" + e.getMessage());
} finally {
    CommonUtils.closeResources(table, connection);
}
```

#### bytesToHex(byte[])方法
**签名：** `public static String bytesToHex(byte[] bytes)`  
**功能：** 将字节数组转换为16进制的字符串，每个byte转换成两个16进制字符，小于16的补0，负数的加256（无符号）  
**参数：**
* bytes: 待转换的字节数组

**例子：**  
```java
System.out.println(bytesToHex([33,-15,15])); //21F10F
```

#### sortMapByKey(Map<String, Object>)方法
**签名：** `public static Map<String, Object> sortMapByKey(Map<String, Object> map)`  
**功能：** 返回按key的字典排序的map  
**参数：**
* map: 待排序的Map

#### replaceStringBuffer(StringBuffer, String, String)方法
**签名：** `public static void replaceStringBuffer(StringBuffer sb, String word, String replacement)`  
**功能：** 使用StringBuffer进行字符串替换(只替换第一次出现的)，减少GC和内存占用  
**参数：**
* sb: 需要进行替换操作的的StringBuffer
* word: 被替换的关键词
* replacement: 替换为的字符串

**例子：**  
```java
StringBuffer buf = new StringBuffer("abbbcc");
replaceStringBuffer(buf, "bb", "e");
System.out.println(buf); //aebcc
```

#### notEmptyString(String...)方法
**签名：** `public static boolean notEmptyString(String... strs)`  
**功能：** 判断所有字符串参数都非空（非null且长度不为0）,全部参数都非空时返回true，有一个或以上的参数为空时返回false  
**参数：**  
* strs: 需要进行非空判断的字符串，支持多个参数

**例子：**  
```java
System.out.println(notEmptyString(null, "abc"); //false 
System.out.println(notEmptyString("", "abc"); //false 
System.out.println(notEmptyString(" ", "abc")); //true  

```

#### shuffleArray(T[])方法
**签名：** `public static <T> T[] shuffleArray(T[] source)`  
**功能：** 随机打乱一个数组，返回打乱后的数组  
**参数：**
* source: 需要打乱的数组
* <T>: 数组的类型

**例子：**  
```java
System.out.println(shuffleArray([1,2,3,4])); //[4,2,3,1] ——其中一种可能的返回结果
```

#### getRandomID()方法
**签名：** `public static String getRandomID()`  
**功能：** 返回一个随机的UUID，长度为32位，每一位都是16进制的字符(0-9a-f)，可以作为数据库记录的ID  
**参数：** <无>

**例子：**  
```java
System.out.println(getRandomID()); //"03c138c242db4eab9b6caec775493db3"——其中一种可能的返回结果
```

#### unicodeDecode(String)方法
**签名：** `public static String unicodeDecode(String s)`  
**功能：** 对进行了Unicode编码的字符串进行解码  
**参数：**  
* s: Unicode编码的字符串

**例子：**  
```java
System.out.println(unicodeDecode("\u6d4b\u8bd5")); //"测试"
```

---
### FileUtils类
文件操作相关的类
#### writeFile(String, String)方法
**签名：** `public static void writeFile(String Path, String data)`  
**功能：** 将字符串data的内容写入到指定文件中  
**参数：**  
* Path: 需写入的文件路径
* data: 需写入的内容

**例子：**  
```java
writeFile("/home/leibniz/1.txt", "Hello,World");
```

#### ReadFile(String)方法
**签名：** `public static String ReadFile(String Path)`  
**功能：** 从Path指定的路径读取文件到字符串中并返回  
**参数：**  
* Path: 需读取的文件路径

**例子：**  
```java
System.out.println(ReadFile("/home/leibniz/1.txt")) //"Hello,World"
```

---
### NetworkUtils类
网络操作相关的工具类
#### postRequestWithData(String, String, String)方法
**签名：** `public static String postRequestWithData(String url, String data, String type) throws IOException`  
**功能：** 使用POST请求执行HTTP接口URL，带请求内容，获取其返回值字符串  
**参数：**  
* url:  需要调用的接口URL，需支持POST请求
* data: 请求体内容，内容格式需与type参数相符
* type: 对应HTTP请求头的ContentType，可以取"json"或"xml"

#### getRequestWithoutData(String)方法
**签名：** `public static String getRequestWithoutData(String url)`  
**功能：** 使用POST请求执行HTTP接口URL，无请求体内容，获取其返回值字符串  
**参数：**  
* url:  需要调用的接口URL，需支持POST请求

#### getRandomIPAddress()方法
**签名：** `public static String getRandomIPAddress()`  
**功能：** 获取一个随机的IP地址  
**参数：** <无>  

**例子：**  
```java
System.out.println(getRandomIPAddress()); //134.245.231.164——其中一种可能的返回结果
```

#### downloadFile(String, File)方法
**签名：** `public static void downloadFile(String url, File file)`  
**功能：** 下载网络文件到指定文件  
**参数：**  
* url: 网络文件地址  
* file: 指定文件对象  

**例子：**  
```java
NetworkUtils.downloadFile("http://turing.asia/images/index/logo.png", new File("~/logo.png"))); //下载图灵LOGO（透明+白字）到本地  
```

---
## encrypt包
加密相关的工具类
### MD5Utils类
直接获取String型的MD5码。
#### getMD5(String)方法
**签名：** `public static String getMD5(String message)`  
**功能：** 获取message的MD5码，直接返回String型（JDK中的方法调用麻烦且返回byte数组）。  
**参数：**  
* message: 需计算MD5的字符串

**例子：**  
```java
System.out.println(MD5Utils.getMD5("Hello，World")); //C500D7D9C8A39834D88B74D3C6943499
```

---
### SHA1Utils类
SHA1码相关工具类，包括获取SHA1码、及用于签名验证的参数排序方法。
#### SHA1(String)方法
**签名：** `public static String SHA1(String decrypt) throws DigestException`  
**功能：** 获取message的SHA1码，直接返回String型（JDK中的方法调用麻烦且返回byte数组）。  
**参数：**  
* message: 需计算SHA1的字符串

**例子：**
```java
System.out.println(SHA1Utils.SHA1("Hello，World")); //dc52f79edbeeef3d069acfbaf5a309f9f2aee9a1
```

#### getOrderByLexicographic(Map<String, Object>)方法
**签名：** `public static String getOrderByLexicographic(Map<String, Object> maps)`  
**功能：** 用于签名验证，将maps中元素按key进行字典序排序，然后拼接成字符串并返回  
**参数：**  
* maps: 用于计算的参数Map

**例子：** 
```java
Map<String, Object> map = new HashMap<>();
map.put("aaa","123");
map.put("zzz","456");
map.put("ggg","789");
System.out.println(map); //{aaa=123, ggg=789, zzz=456}
System.out.println(SHA1Utils.getOrderByLexicographic(map)); //aaa123ggg789zzz456
```

### MurmurHash3Utils类
MurmurHash算法工具类，包括使用MurmurHash3算法获取32和64位哈希值的方法。
#### hash3_x86_32(String)方法
**签名：** `public static int hash3_x86_32(String data)`  
**功能：** 用MurmurHash3算法获取32位哈希值，针对x86系统进行了优化。  
**参数：**  
* data: 需计算哈希值的字符串

**例子：**  
```java
System.out.println(MurmurHash3Utils.hash3_x86_32("aaaaaaa1"));//-321361167
System.out.println(MurmurHash3Utils.hash3_x86_32("aaaaaaa2"));//-55224788
```

#### hash3_x64_64(String)方法
**签名：** `public static long hash3_x64_64(String data)`  
**功能：** 用MurmurHash3算法获取64位哈希值，针对x64系统进行了优化。  
**参数：**  
* data: 需计算哈希值的字符串

**例子：**  
```java
System.out.println(MurmurHash3Utils.hash3_x64_64("aaaaaaa1"));//-7080421957793229055
System.out.println(MurmurHash3Utils.hash3_x64_64("aaaaaaa2"));//-8531802624573505039
```

---
## format包
格式转换的工具类
### DateUtils类
提供获取常见日期格式字符串的方法，以及获取N小时前的时刻的方法。
#### getSimpleHour(Date)方法
**签名：** `public static String getSimpleHour(Date date)`  
**功能：** 获取"HH:00"格式的时间字符串  
**参数：**  
* date: 需转换格式的Date对象

**例子：**
```java
System.out.println(DateUtils.getSimpleHour(new Date())); //"16:00"——其中一种可能的返回结果
```

#### getSimpleDate(Date)方法
**签名：** `public static String getSimpleDate(Date date)`  
**功能：** 获取"yyyy-MM-dd"格式的时间字符串  
**参数：**  
* date: 需转换格式的Date对象

**例子：**
```java
System.out.println(DateUtils.getSimpleDate(new Date())); //"2017-01-10"——其中一种可能的返回结果
```

#### getDate(Date)方法
**签名：** `public static String getDate(Date date)`  
**功能：** 获取"yyyy-MM-dd HH: mm:ss"格式的时间字符串  
**参数：**  
* date: 需转换格式的Date对象

**例子：**
```java
System.out.println(DateUtils.getDate(new Date())); //"2016-01-10 16:18:34"——其中一种可能的返回结果
```

#### getDate_day(Date)方法
**签名：** `public static String getDate_day(Date date)`  
**功能：** 获取"dd-HH"格式的时间字符串  
**参数：**  
* date: 需转换格式的Date对象

**例子：**
```java
System.out.println(DateUtils.getDate_day(new Date())); //"10-16"——其中一种可能的返回结果
```

#### getHoursAgo(Date, int)方法
**签名：** `public static String getHoursAgo(Date date, int num)`  
**功能：** 获取在date的num个小时之前时刻的"yyyy-MM-dd HH: mm:ss"格式的时间字符串  
**参数：**  
* date: 需转换格式的Date对象
* num: 多少个小时之前

**例子：**
```java
System.out.println(DateUtils.getHoursAgo(new Date(), 24)); //"2017-01-09 16:20:16"——获取一天前的时刻，其中一种可能的返回结果
```
---
### ExcelUtils类
提供写入数据到Excel文件的方法。
#### createXls(OutputStream, String, String[], Object[][], String)方法
**签名：** `public static void createXls(OutputStream fos, String title, String[] columnName, Object[][] data, String reportDate)`  
**功能：** 将数据以指定格式写入到(文件)输出流。  
**参数：**  
* title: 表单的标题
* columnName: 列名，String数组，长度应该对应data的列数
* data: 数据,Object二维数组，对应Excel表数据的行和列
* reportDate: 查询的日期范围

---
### XmlUtils类
提供将键值对数据转换为xml格式的字符串的方法。
#### simpleMapToXml(Map<String, Object>)方法
**签名：** `public static String simpleMapToXml(Map<String, Object> map)`  
**功能：** 将Map数据键值对以xml格式输出（UTF-8编码），根元素为xml标签，每个键值对作为一个子标签  
**参数：**  
* map: 需要转换为xml格式的键值对数据  

**例子：**
```java
Map<String, Object> map = new HashMap<>();
map.put("aaa","123");
map.put("zzz","456");
map.put("ggg","789");
System.out.println(XmlUtils.simpleMapToXml(map)); //<xml><aaa>123</aaa><ggg>789</ggg><zzz>456</zzz></xml>
```

#### simpleMapToXml(Map<String, Object>, String)方法  
**签名：** `public static String simpleMapToXml(Map<String, Object> map, String encode)`  
**功能：** 与simpleMapToXml(Map<String, Object>)相同，区别在于可以指定返回字符串的编码  
**参数：**  
* map: 需要转换为xml格式的键值对数据  
* encode: 返回字符串的编码  

#### xml2Object(String, Class<T>)方法
**签名：** `public static <T> T xml2Object(String xmlStr, Class<T> clazz)`  
**功能：** 将xml格式的字符串转换为指定类型的对象  
**参数：**  
* xmlStr: 需要解析的xml字符串
* clazz: 需要解析成的实体类class字节码对象，需要在标签上增加@XmlRootElement和@XmlElement注解

**例子：**
```java
/**
 * 实体类
 * 对应xml示例：
 *    <?xml version="1.0" encoding="utf-8" ?> 
 *    <messStatus>
 *        <error>0</error> 
 *        <receipt>
 *            <preceipt>
 *                <phone>13932086576</phone> 
 *                <msg_flag>2</msg_flag> 
 *            </preceipt>
 *            <preceipt>
 *                <phone>13932086576</phone> 
 *                <msg_flag>2</msg_flag> 
 *            </preceipt>
 *        </receipt>
 *    </messStatus>
 * 篇幅受限，省略掉toString()方法了
 */
//根标签加@XmlRootElement注解
@XmlRootElement(name = "messStatus")
public class MessageReport {
    //错误信息
    private String error;
    //储存每条记录
    private List<Preceipt> receipt;

    //普通标签在get方法前加@XmlElement注解
    @XmlElement() //如果字段名/get的名字与xml标签名一致，可以不写name属性
    public String getError() {return error;}
    public void setError(String error) {this.error = error;}

    //多个标签组成数组的，在get方法前加@XmlElementWrapper注解
    @XmlElementWrapper() //如果字段名/get的名字与xml标签名一致，可以不写name属性
    @XmlElement(name = "preceipt")//标签数组中的每个标签在get方法前对应@XmlElement注解，必须填name属性，指定子标签名
    public List<Preceipt> getReceipt() {return receipt;}
    public void setReceipt(List<Preceipt> receipt) {this.receipt = receipt;}
    //储存单条记录
    private static class Preceipt {
        //手机号码
        private String phone;
        //回执状态2代表成功回执，3代表失败回执
        private Integer msg_flag;


        @XmlElement() //如果字段名/get的名字与xml标签名一致，可以不写name属性
        public String getPhone() {return phone;}
        public void setPhone(String phone) {this.phone = phone;}

        @XmlElement() //如果字段名/get的名字与xml标签名一致，可以不写name属性
        public Integer getMsg_flag() {return msg_flag;}
        public void setMsg_flag(Integer msg_flag) {this.msg_flag = msg_flag;}
    }
}
/**
 * 调用该方法解析的示例
 */
public class SMTest {
    @Test
    public void xmlParseTest() {
        //对应上面实体类前面注释的xml示例
        String xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><messStatus><error>0</error><receipt>\n" +
                "<preceipt><phone>13932086576</phone><msg_flag>2</msg_flag></preceipt>\n" +
                "<preceipt><phone>13932086576</phone><msg_flag>2</msg_flag></preceipt>\n" +
                "</receipt></messStatus>\n";
        MessageReport report = XmlUtils.xml2Object(xmlStr, MessageReport.class);
        System.out.println(report);//输出结果：MessageReport{error='0', receipt=[Preceipt{phone='13932086576', msg_flag=2}, Preceipt{phone='13932086576', msg_flag=2}]}
    }
}
```

---
## wechat包
微信接口调用的工具类
### QRCodeUtils类
二维码的工具类，提供生成二维码图片文件/输出流的方法。  
#### writeToFile(String, int, File)方法
**签名：** `public static void writeToFile(String data, int side, File file)`  
**功能：** 将指定数据生成二维码并写入到指定文件中  
**参数：**  
* data: 二维码中的数据
* side: 二维码(正方形)的边长，像素
* file: 写入的文件

#### writeToStream(String, int, OutputStream)方法
**签名：** `public static void writeToStream(String data, int side, OutputStream stream)`  
**功能：** 将指定数据生成二维码并写入到指定输出流中  
**参数：**  
* data: 二维码中的数据
* side: 二维码(正方形)的边长，像素
* stream: 写入的输出流

---
### WechatRecharge类
微信充值的工具类，提供调用话费充值、订单查询、余额查询的方法。  
#### rechargeAccount(String, int, int, int, String, String, String)方法
**签名：** `public static String rechargeAccount(String accountVal, int merchant, int clientId, int product, String outTradeNo, String version, String key) throws IOException`  
**功能：** 调用手机充值接口  
**参数：**  
* accountVal: 充值目标账号（手机号）
* merchant: 商户号
* clientId: 服务接口编号
* product: 充值产品编号
* outTradeNo: 商户本地订单号
* version: 接口版本号：V100
* key: 签名

#### searchOrder(int, int, String, String, String)方法
**签名：** `public static String searchOrder(int merchant, int clientId, String outTradeNo, String version, String key) throws IOException`  
**功能：** 调用查询订单接口  
**参数：**  
* merchant: 商户号
* clientId: 服务接口编号
* outTradeNo: 商户本地订单号
* version: 接口版本号：V100
* key: 签名

#### searchOrder(int, int, String, String, String)方法
**签名：** `public static String searchBalance(int merchant, int clientId, String version, String key) throws IOException`  
**功能：** 调用查询余额接口  
**参数：**  
* merchant: 商户号
* clientId: 服务接口编号
* version: 接口版本号：V100
* key: 签名

---
### WaterMarkUtils类
添加水印的工具类，添加水印文字和水印图片的方法。  
#### waterMark(WaterMarkParam waterMarkParam)方法
**签名：** `public static boolean waterMark(WaterMarkParam waterMarkParam)`  
**功能：** 添加水印文字和水印图片的接口  
**参数：**  
* waterMarkParam: 添加水印参数的封装对象，详情查看WaterMarkParam类的属性说明

---
### WaterMarkParam类
添加水印参数的封装类，下面对各个私有属性进行说明。  
#### 类属性说明
* sourceFile: 需要添加水印的模板图片文件，这个文件必须存在
* waterMarkText: 需要添加的水印文字对象，与水印图片文件、水印图片Image必须至少存在一个
* waterMarkTextSize: 水印文字的大小，可为空，空时默认为60
* waterMarkTextFont: 水印文字的字体，可为空，空时默认为微软雅黑 粗体，大小见waterMarkTextSize属性
* waterMarkTextColor: 水印文字的颜色，可为空，空时默认为白色
* waterMarkTextX: 水印文字左下角的X坐标，可为空，空时默认水平居中，该值只在水印文字存在时有效
* waterMarkTextY: 水印文字左下角的Y坐标，可为空，空时默认垂直居中，该值只在水印文字存在时有效
* waterMarkFile: 需要添加的水印图片(File格式)，与水印文字对象、水印图片Image必须至少存在一个,waterMarkImage存在时该值无效
* waterMarkImage: 需要添加的水印图片(Image格式)，与水印文字对象、水印图片文件必须至少存在一个
* waterMarkFileX: 水印图片的左上角的X坐标，可为空，空时默认水平居中，该值只在水印图片存在时有效
* waterMarkFileY: 水印图片的左上角的Y坐标，可为空，空时默认垂直居中，该值只在水印图片存在时有效
* targetPath: 目标文件的保存目录，不包含文件名，可以以“/”结尾，没有时会自动加上，是必须参数
* targetFileName: 目标文件的文件名称，不包含后缀名，不能以“.”开头，是必须参数
* targetType: 目标文件的类型，只能填png或jpg，其他情况均默认png格式
