package com.turingdi;

import com.turingdi.commonutils.encrypt.MurmurHash3Utils;
import org.junit.Test;

/**
 * Encrypt包测试类
 * Created by leibniz on 2017-01-23.
 */
public class EncryptUtilsTest {
    @Test
    public void testMurmurHash(){
        System.out.println(MurmurHash3Utils.hash3_x64_64("aaaaaaa1"));
        System.out.println(MurmurHash3Utils.hash3_x64_64("aaaaaaa2"));
        System.out.println(MurmurHash3Utils.hash3_x86_32("aaaaaaa1"));
        System.out.println(MurmurHash3Utils.hash3_x86_32("aaaaaaa2"));
    }
}
