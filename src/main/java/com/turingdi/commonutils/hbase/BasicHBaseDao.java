/*
package com.turingdi.commonutils.hbase;

import com.turingdi.commonutils.basic.CommonUtils;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

*/
/**
 * 封装常用的HBase增删改差方法，dao类继承之即可
 * Created by leibniz on 16-12-21.
 *//*

@SuppressWarnings("unused")
public class BasicHBaseDao {
    private static Logger LOG = Logger.getLogger(BasicHBaseDao.class);
    private String tableName;

    public BasicHBaseDao(String tableName) {
        this.tableName = tableName;
    }

    */
/**
     * 往hbase中录入数据，通过HBaseUtils的getConnection静态方法
     * 获取与hbase的连接，并进一步获得表实例，对表进行录入数据之后，将表关闭
     * 并调用HBaseUtils的closeConnection静态方法释放连接资源
     * 该方法用于一次录入多条数据
     *
     * @param entities put对象，里面存储要录入的数据 return type: void write time: 下午1:46:14
     *//*

    public void insert(List<HBaseEntity> entities) {
        Connection connection = null;
        Table table = null;
        try {
            connection = HBaseConnectionUtils.getConnection();// 获取hbase连接
            table = connection.getTable(TableName.valueOf(this.tableName));// 获取table 对象
            List<Put> puts = new ArrayList<>();
            entities.stream().map((entity) -> changeToPut(entity.getRowKey(), entity.toMap())).forEach(puts::add);
            table.put(puts);
        } catch (IOException e) {
            LOG.info("打开HBase表时抛出异常：" + e.getMessage());
        } finally {
            CommonUtils.closeResources(table, connection);
        }
    }

    */
/**
     * 重载insert方法，用于录入单条数据
     *//*

    public void insert(HBaseEntity entity) {
        Connection connection = null;
        Table table = null;
        try {
            connection = HBaseConnectionUtils.getConnection();// 获取hbase连接
            table = connection.getTable(TableName.valueOf(this.tableName));// 获取table 对象
            Put put = changeToPut(entity.getRowKey(), entity.toMap());
            table.put(put);
        } catch (IOException e) {
            LOG.info("打开HBase表时抛出异常：" + e.getMessage());
        } finally {
            CommonUtils.closeResources(table, connection);
        }
    }

    */
/**
     * 根据get获取数据,将数据封装到hashmap里, 并通过java反射机制获取泛型类的带参构造,将hashmap
     *
     * @param rowkey Rowkey
     * @param clazz   泛型
     * @return 泛型类的实例 return type: T write time: 下午5:18:22
     * 传入该带参构造,之后取得该泛型类的实例并返回
     *//*

    public <T> T select(String rowkey, Class<T> clazz) {
        Connection connection = null;
        Table table = null;
        Map<String, String> map = new HashMap<>();
        try {
            connection = HBaseConnectionUtils.getConnection();// 连接hbase
            table = connection.getTable(TableName.valueOf(this.tableName));
            Get get = new Get(Bytes.toBytes(rowkey));
            Result result = table.get(get);
            if (result != null && result.rawCells().length > 0) {
                // 遍历cell，查询结果赋值到map里
                for (Cell cell : result.rawCells())
                    map.put(new String(CellUtil.cloneQualifier(cell)), new String(CellUtil.cloneValue(cell)));
                // 通过反射机制获取泛型对象的带参构造，并获得对象的实例,并返回实例
                return clazz.getConstructor(Map.class).newInstance(map);
            }
        } catch (IOException e) {
            LOG.error("打开HBase表时抛出异常：" + e.getMessage());
            return null;
        } catch (ReflectiveOperationException e) {
            LOG.error("通过反射建立实例时抛出异常：" + e.getMessage());
            return null;
        } finally {
            CommonUtils.closeResources(table, connection);
        }
        return null;
    }

    */
/**
     * 判断在tableName表中是否存在某主键
     *
     * @param rowkey Rowkey
     * @return 存在该主键, 则返回true, 反之false
     *//*

    public boolean isRowKeyExists(String rowkey) {
        Connection connection = null;
        Table table = null;
        try {
            Get get = new Get(Bytes.toBytes(rowkey));
            connection = HBaseConnectionUtils.getConnection();// 获取hbase连接
            table = connection.getTable(TableName.valueOf(this.tableName));
            return !table.get(get).isEmpty();
        } catch (IOException e) {
            LOG.error("打开HBase表时抛出异常：" + e.getMessage());
        } finally {
            CommonUtils.closeResources(table, connection);
        }
        return false;
    }

    */
/**
     * 对新建一个put的方法进行封装,避免每次都要使用很冗长的Bytes.toBytes
     *
     * @param rowKey    主键
     * @param family    列族
     * @param qualifier 列名
     * @param value     值
     * @return Put 对象
     *//*

    private static Put changeToPut(String rowKey, String family, String qualifier, String value) {
        // 参数合法性验证
        if (CommonUtils.notEmptyString(rowKey, family, qualifier, value)) {
            Put put = new Put(Bytes.toBytes(rowKey));
            put.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier), Bytes.toBytes(value));
            return put;
        }
        return null;
    }

    */
/**
     * put中有多个列族或多个列名与值,使用Map是为了防止有null键或值
     *
     * @param rowKey    主键
     * @param entityMap Map<列族, Map<列名,值>>
     * @return Put对象
     *//*

    private static Put changeToPut(String rowKey, Map<String, Map<String, String>> entityMap) {
        if (CommonUtils.notEmptyString(rowKey)) {
            Put put = new Put(rowKey.getBytes());
            entityMap.entrySet().forEach(
                    (entry) -> entry.getValue().entrySet().forEach(
                            (entryQV) -> put.addColumn(entry.getKey().getBytes(), entryQV.getKey().getBytes(), entryQV.getValue().getBytes())
                    )
            );
            return put;
        }
        return null;
    }
}
*/
