/*
package com.turingdi.commonutils.hbase;

import com.turingdi.commonutils.basic.PropertiesUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.log4j.Logger;

import java.io.IOException;

*/
/**
 * 用于获取HBase连接，创建Put对象
 * Created by leibniz on 16-12-21.
 *//*

public class HBaseConnectionUtils {
    private static Configuration conf;
    private static Logger LOG = Logger.getLogger(HBaseConnectionUtils.class);

    */
/*
     * 静态代码快加载配置文件
     *//*

    static {
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", PropertiesUtils.getString("hbase", "ZKQuorum"));
        conf.set("zookeeper.znode.parent", PropertiesUtils.getString("hbase","ZKNodeParent"));
    }

    */
/**
     * 获取连接对象
     *
     * @return Connection对象
     * @author Leibniz
     *//*

    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = ConnectionFactory.createConnection(conf);
        } catch (IOException e) {
            LOG.error("获取HBase连接时抛出IO异常" + e.getMessage());
        }
        return connection;
    }
}
*/
