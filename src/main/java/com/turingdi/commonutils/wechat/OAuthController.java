package com.turingdi.commonutils.wechat;

import com.turingdi.commonutils.basic.NetworkUtils;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 普通用户注册
 * <p>
 * Created by leibniz on 2017-03-16.
 */
public abstract class OAuthController {
    private static Logger LOG = LoggerFactory.getLogger(OAuthController.class);//使用slf4j日志框架

    //授权完成后，跳转到原访问地址时，需要去掉的请求参数（无需填写"code"，"state"，"visitUrl"
    private List<String> REMOVE_PARAMS = new ArrayList<>();

    //当授权后没有拿到原访问地址时，应该跳到的默认视图
    private String defaultViewAfterOAuth;

    //我们的公众号授权回调地址,同时获取OpenId和用户信息
    private String userInfoCallback;

    /**
     * 默认构造方法
     *
     * @param removeParams          授权完成后，跳转到原访问地址时，需要去掉的请求参数
     * @param defaultViewAfterOAuth 当授权后没有拿到原访问地址时，应该跳到的默认视图
     * @param userInfoCallback      授权Controller的请求地址，从“http”开始
     */
    public OAuthController(List<String> removeParams, String defaultViewAfterOAuth, String userInfoCallback) {
        if (removeParams != null) {
            REMOVE_PARAMS.addAll(removeParams);
        }
        this.defaultViewAfterOAuth = defaultViewAfterOAuth;
        this.userInfoCallback = userInfoCallback + "/getUserInfo?visitUrl=";
    }

    /**
     * 微信公众号授权URL，scope=snsapi_userinfo，可用于获取用户信息
     */
    private final static String OAUTH_INFO_API = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_userinfo#wechat_redirect";

    /**
     * 微信公众号授权URL，scope=snsapi_base，只能用于获取用户OpenID
     */
    final static String OAUTH_BASE_API = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base#wechat_redirect";

    /**
     * 微信公众号获取OpenID的API地址
     */
    private static final String OPENID_API = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

    /**
     * 微信公众号获取用户信息的API地址
     */
    private static final String USERINFO_API = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";

    /**
     * 微信接口的回调方法,用于获取用户OpenID以及用户基本信息,并保存到数据库
     * 同时，判断用户对于当前企业公众号是否首次授权，以维护活动的新增授权数，以及用户信息中的首次授权活动ID字段
     *
     * @param request http请求对象
     * @param code    微信传入的code
     * @return View路径，授权前用户访问页面
     *
     * @author Leibniz
     */
    @RequestMapping("getUserinfo")
    public String getUserInfo(HttpServletRequest request, String code) {
        try {
            LOG.debug("code={},RemoteAddr={},RemoteHost={}", new Object[]{code, request.getRemoteAddr(), request.getRemoteHost()});
            assert code != null;
            String openIdUrl = String.format(OPENID_API, getAppIdFromRequest(request), getAppSecretFromRequest(request), code);
            JSONObject openIdJson = JSONObject.fromObject(NetworkUtils.postRequestWithData(openIdUrl, null, null));
            LOG.debug("授权返回的json数据：{}", openIdJson);
            if (openIdJson.containsKey("openid")) {
                String openId = openIdJson.getString("openid");
                //如果数据库中没有当前用户信息，则调用用户基本信息接口获取之
                if (judgeNeedGetUserinfo(request, openId)) {
                    //微信获取用基本信息接口
                    JSONObject userInfoJson = null;
                    try {
                        String userinfo_url = String.format(USERINFO_API, openIdJson.getString("access_token"), openId);
                        String strJson = NetworkUtils.postRequestWithData(userinfo_url, null, null);
                        userInfoJson = JSONObject.fromObject(strJson);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (null != userInfoJson) {
                        LOG.info("当前授权的用户信息:{}", userInfoJson.toString());
                        WechatUserInfo wechatUser = WechatUserInfo.fromOAuthJson(userInfoJson);
                        processAfterGotUserInfo(request, wechatUser);
                    }
                } else {
                    processAfterGotOpenId(request, openId);
                }

                //重定向到原访问URL
                String visitUrl = getRedirectAddr(request, REMOVE_PARAMS);
                if (visitUrl.length() > 0) {
                    try {
                        return "redirect:" + visitUrl;
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            } else if (openIdJson.containsKey("errcode")) {
                //有错误
                return "redirect:" + getErrorUrl(request, openIdJson.getString("errmsg"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //跳到默认视图
        return "forward:" + defaultViewAfterOAuth;
    }

    /**
     * 获取到微信用户详细信息后处理方法
     * 可以是保存用户信息到数据库，保存OpenID到Session等操作
     *
     * @param request    HTTP请求对象，视乎具体的业务需求，可能用不着
     * @param wechatUser 微信用户对象
     * @author Leibniz
     */
    protected abstract void processAfterGotUserInfo(HttpServletRequest request, WechatUserInfo wechatUser);

    /**
     * 获取到微信用户OpenID，不需要进一步获取详细信息时的处理方法
     * 可以是保存OpenID到Session等操作
     *
     * @param request HTTP请求对象，视乎具体的业务需求，可能用不着
     * @param openId  当前授权的用户微信OpenID
     * @author Leibniz
     */
    protected abstract void processAfterGotOpenId(HttpServletRequest request, String openId);

    /**
     * 判断当前微信是否需要获取详细信息
     * 一般来说微信用户详细信息保存在数据库中，根据openID查找是否存在记录，不存在的话就需要获取（返回true）
     *
     * @param request HTTP请求对象，视乎具体的业务需求，可能用不着
     * @param openId  当前授权的用户微信OpenID
     * @return true=需要获取详细信息， false=不获取详细信息
     *
     * @author Leibniz
     */
    protected abstract boolean judgeNeedGetUserinfo(HttpServletRequest request, String openId);

    /**
     * 获取微信AppID
     * 这里提供Http请求对象，可能具体业务需求根据请求的页面（如活动）查找对应的AppID
     * 也可能具体业务是忽略请求者/请求内容，直接从数据库/配置文件读取AppID
     *
     * @param request http请求对象
     * @return 微信AppID
     *
     * @author Leibniz
     */
    protected abstract String getAppSecretFromRequest(HttpServletRequest request);

    /**
     * 获取微信AppSecret
     * 这里提供Http请求对象，可能具体业务需求根据请求的页面（如活动）查找对应的AppSecret
     * 也可能具体业务是忽略请求者/请求内容，直接从数据库/配置文件读取AppSecret
     *
     * @param request http请求对象
     * @return 微信AppID
     *
     * @author Leibniz
     */
    protected abstract String getAppIdFromRequest(HttpServletRequest request);

    /**
     * 获取授权错误处理页面的URI，将会在授权出错时重定向到改地址
     *
     * @param request http请求对象
     * @param errmsg  微信授权错误信息
     * @return 授权错误处理页面
     *
     * @author Leibniz
     */
    protected abstract String getErrorUrl(HttpServletRequest request, String errmsg);

    /**
     * 微信接口的回调方法,用于获取用户OpenID，不获取用户详细信息。
     * 如果数据库中没有该OpenID的信息，将跳转到获取用户详细信息的方法，否则跳转到授权前访问页面
     *
     * @param request http请求对象
     * @param code    微信传入的code
     * @return ModelAndView，授权前访问页面，或者获取用户详细信息
     *
     * @author Leibniz
     */
    @RequestMapping("getOpenId")
    public String getOpenId(HttpServletRequest request, String code) {
        try {
            LOG.debug("code={},RemoteAddr={},RemoteHost={}", new Object[]{code, request.getRemoteAddr(), request.getRemoteHost()});
            assert code != null;
            String openIdUrl = String.format(OPENID_API, getAppIdFromRequest(request), getAppSecretFromRequest(request), code);
            JSONObject openIdJson = JSONObject.fromObject(NetworkUtils.postRequestWithData(openIdUrl, null, null));
            LOG.debug("授权返回的json数据：{}", openIdJson);
            //重定向到原访问URL
            if (openIdJson.containsKey("openid")) {
                String openId = openIdJson.getString("openid");
                //如果数据库中没有当前用户信息，则重定向到scope=snsapi_userinfo的接口获取之
                if (judgeNeedGetUserinfo(request, openId)) {
                    String visitUrl = getRedirectAddr(request, null);
                    String oauthUrl = String.format(OAUTH_INFO_API, getAppIdFromRequest(request), URLEncoder.encode(userInfoCallback + visitUrl, "UTF-8"));
                    return "redirect:" + oauthUrl;
                } else {
                    processAfterGotOpenId(request, openId);
                }
                String visitUrl = getRedirectAddr(request, REMOVE_PARAMS);
                if (visitUrl.length() > 0) {
                    return "redirect:" + visitUrl;
                }
            } else if (openIdJson.containsKey("errcode")) {
                //有错误
                return "redirect:" + getErrorUrl(request, openIdJson.getString("errmsg"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //跳到默认视图
        return "forward:" + defaultViewAfterOAuth;
    }

    /**
     * 从请求中删掉不需要的请求参数，保留需要的，返回处理后的授权前请求路径
     *
     * @param request      Http请求对象
     * @param removeParams 需要删除的请求参数
     * @return 授权前请求路径，已去掉多余的请求参数
     *
     * @author Leibniz
     */
    private String getRedirectAddr(HttpServletRequest request, List<String> removeParams) {
        String visitUrl = request.getParameter("visitUrl");
        Map<String, String[]> parameterMap = new HashMap<>(request.getParameterMap());
        if (removeParams != null) {
            for (String param : removeParams) {
                parameterMap.remove(param);
            }
        }
        parameterMap.remove("code");
        parameterMap.remove("state");
        parameterMap.remove("visitUrl");
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()[0]).append("&");
        }

        if (sb.length() != 0) {
            sb.deleteCharAt(sb.length() - 1);
        }

        String split = visitUrl.contains("?") ? "&" : "?";
        return visitUrl + split + sb.toString();
    }
}
