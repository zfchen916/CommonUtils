package com.turingdi.commonutils.wechat.example;

import com.turingdi.commonutils.basic.PropertiesUtils;
import com.turingdi.commonutils.wechat.OAuthController;
import com.turingdi.commonutils.wechat.WechatUserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.turingdi.commonutils.wechat.example.OAuthFilterExample.SESSION_OPENID_KEY;

/**
 * Created by leibniz on 2017-08-15.
 */
@Controller
@RequestMapping("oauthExample")
public class OAuthControllerExample extends OAuthController {

    public OAuthControllerExample() {
        super(null, //授权后跳转时，没有额外需要去掉的参数
                "/personalCenter/show", //授权后没有拿到原访问地址时，跳到个人中心
                PropertiesUtils.getString("wechat", "PROJECT_URL") + "oauthExample");
    }

    @Override
    protected void processAfterGotUserInfo(HttpServletRequest request, WechatUserInfo wechatUser) {
        request.getSession().setAttribute(SESSION_OPENID_KEY, wechatUser.getOpenid());//保存OpenID到Session
        //TODO 保存用户信息到数据库
    }

    @Override
    protected void processAfterGotOpenId(HttpServletRequest request, String openId) {
        request.getSession().setAttribute(SESSION_OPENID_KEY, openId);//保存OpenID到Session
    }

    @Override
    protected boolean judgeNeedGetUserinfo(HttpServletRequest request, String openId) {
        //TODO 根据openid从数据库读取用户信息
        //  WechatUserInfo user = wechatUserService.selectByOpenId(openId);
        //  return user == null || user.getHeadimgurl() == null || user.getNickname() == null; //没记录、或者头像/昵称没获取到的时候需要调用接口获取详细信息
        return false;
    }

    @Override
    protected String getAppSecretFromRequest(HttpServletRequest request) {
        return PropertiesUtils.getString("wechat", "appsecret");
    }

    @Override
    protected String getAppIdFromRequest(HttpServletRequest request) {
        return PropertiesUtils.getString("wechat", "appid");
    }

    @Override
    protected String getErrorUrl(HttpServletRequest request, String errmsg) {
        return "/errorHandler/oauthError?errmsg=" + errmsg;
    }
}
