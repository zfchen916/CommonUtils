package com.turingdi.commonutils.wechat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
public final class QRCodeUtils {

    private static final int BLACK = 0xFF000000;
    private static final int WHITE = 0xFFFFFFFF;

    private static final Logger LOG = Logger.getLogger(QRCodeUtils.class);

    private QRCodeUtils() {
    }

    private static BufferedImage toBufferedImage(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
            }
        }
        return image;
    }

    public static void writeToFile(String data, int side, File file) {
        writeToFile(preHandle(data, side), "jpg", file);// 将二维码数据写到图片文件中
    }

    public static void writeToStream(String data, int side, OutputStream stream) {
        writeToStream(preHandle(data, side), "jpg", stream);// 将二维码数据写到流中
    }

    public static Image getImage(String data, int side){
        return toBufferedImage(preHandle(data, side));
    }

    private static void writeToFile(BitMatrix matrix, String format, File file) {
        try {
            writeToStream(matrix, format, new FileOutputStream(file));
        } catch (FileNotFoundException e) {
            LOG.error("找不到指定的文件：" + e.getMessage());
        }
    }

    private static void writeToStream(BitMatrix matrix, String format, OutputStream stream) {
        BufferedImage image = toBufferedImage(matrix);
        try {
            if (!ImageIO.write(image, format, stream)) {
                LOG.error("写入二维码图片失败");
            }
        } catch (IOException e) {
            LOG.error("写入二维码图片时抛出IO异常：" + e.getMessage());
        }
    }

    // 生成二维码矩阵
    private static BitMatrix preHandle(String data, int side){
        BitMatrix result;
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();// 创建MultiFormatWriter对象
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>(){
		{
			put(EncodeHintType.CHARACTER_SET, "UTF-8"); // 设置编码类型
			put(EncodeHintType.MARGIN, 1); // 设置白边大小
		}
	};// 创建Map对象，用于保存二维码基本信息，并设置编码
        try {
            result = multiFormatWriter.encode(data, BarcodeFormat.QR_CODE, side, side, hints);// 生成二维码矩阵
        } catch (WriterException e) {
            LOG.error("二维码编码时抛出异常：" + e.getMessage());
            return null;
        }
        return result;
    }
}
